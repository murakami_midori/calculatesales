package jp.alhinc.murakami_midori.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CalculateSales {
	public static void main(String[] args) {

		//マップの準備
		HashMap<String, String> branchMap = new HashMap<>();
		HashMap<String, Long> salesMap = new HashMap<>();
		BufferedReader bufferedReader = null;

		//ファイルの有無の処理
			if(args.length != 1) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}
			try {
			File file = new File(args[0], "branch.lst");
			if (!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}


			//ファイルの中身を読み込んでマップに保持
			FileReader fileReader = new FileReader(file);
			bufferedReader = new BufferedReader(fileReader);
			String branchs;
			while ((branchs = bufferedReader.readLine()) != null) {

				String[] str = branchs.split(",");
				if((str.length != 2) || !str[0].matches("([0-9]{3})")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				branchMap.put(str[0], str[1]);
				Long l = 0L;
				salesMap.put(str[0], l);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (IOException e) {
					return;
				}
			}
		}


		String sales; //ファイルの中身文読み込む処理の準備
		BufferedReader br = null;
		File dir = new File(args[0]);
		File[] list = dir.listFiles();
		List<String> fileLst = new ArrayList<>();
		String fileName = null;
		String fileNumber;
		String fileNumber1;

		//条件に合うファイル名のみリストに保持
		for (int i = 0; i < list.length; i++) {
			fileName = list[i].getName();
			if (list[i].isFile() && fileName.matches("[0-9]{8}.rcd")) {
				fileLst.add(fileName);
			}
		}

		for (int i = 0; i < fileLst.size()-1; i++) {
			fileNumber = fileLst.get(i).substring(0,8);
			fileNumber1 = fileLst.get(i+1).substring(0,8);
			int a = Integer.parseInt(fileNumber);
			int b = Integer.parseInt(fileNumber1);
			if(a + 1 !=  b) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}


		//条件に合うファイルのみ読み込み中身のフォーマット正誤確認後マップにプット
		for (int i = 0; i < fileLst.size(); i++) {
			try {
				List<String> addLst = new ArrayList<>();
				FileReader fr = new FileReader(list[i]);
				br = new BufferedReader(fr);
				while ((sales = br.readLine()) != null) {
					addLst.add(sales);
				}

				if(addLst.size() != 2) {
					System.out.println(fileLst.get(i) + "のフォーマットが不正です");
					return;
				}

				if(!addLst.get(1).matches("[0-9].*")) {
					System.out.println("予期せぬエラーが発生しました。");
					return;
				}

				if(!salesMap.containsKey(addLst.get(0))) {
					System.out.println(fileLst.get(i) + "の支店コードが不正です");
					return;
				}

				long salse = Long.parseLong(addLst.get(1));
				long addvalue = salse + salesMap.get(addLst.get(0));

				salesMap.get(addLst.get(0));{
					salesMap.put(addLst.get(0),addvalue);

					if(addvalue >= 10000000000L ){
						System.out.println("合計金額が10桁を超えました");
						return;
					}
				}
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						return;
					}
				}
			}
		}

		BufferedWriter bw3 = null;
		try {
			File file = new File(args[0] , "\\branch.out");
			FileWriter fw = new FileWriter(file);
			bw3 = new BufferedWriter(fw);
			for(String code : branchMap.keySet()) {
				bw3.write(code + "," + branchMap.get(code) + "," + salesMap.get(code));
				bw3.newLine();
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if (bw3 != null) {
				try {
					bw3.close();
				} catch (IOException e) {
					return;
				}
			}
		}
	}
}